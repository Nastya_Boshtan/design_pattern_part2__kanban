package com.epam.model.state.impl;

import com.epam.model.Developer;
import com.epam.model.Task;
import com.epam.model.state.State;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Done implements State {

  private static Logger log = LogManager.getLogger(ToDo.class);
  private Task task;

  @Override
  public void delete(Task task) {
    Deleted state = new Deleted();
    state.setStateBeforeDeleting(task.getState());
    state.setTask(task);
    task.setState(state);
    task.setDeveloper(null);
    log.info(String.format(" %s moved to deleted state", task));
  }

  @Override
  public void setDeveloper(Developer developer) {
    this.task.setDeveloper(developer);
    log.info(String.format("Added %s to %s", developer, task));
  }


  @Override
  public String toString() {
    return "Done";
  }
}
