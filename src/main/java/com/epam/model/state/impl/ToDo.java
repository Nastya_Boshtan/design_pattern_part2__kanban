package com.epam.model.state.impl;

import com.epam.model.Developer;
import com.epam.model.Task;
import com.epam.model.enums.Position;
import com.epam.model.state.State;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ToDo implements State {

  private static Logger log = LogManager.getLogger(ToDo.class);
  private Task task;

  @Override
  public void startToDevelop(Task task, Developer developer) {
    if (developer.getPosition() != Position.QA) {
      State state = new Development();
      state.setTask(task);
      if (developer != null) {
        state.setDeveloper(developer);
      }
      task.setState(state);
      log.info(String.format(" %s moved to development state", task));
    } else {
      log.warn("Position must be developer or team lead, but not a QA");
    }
  }

  @Override
  public void delete(Task task) {
    Deleted state = new Deleted();
    state.setStateBeforeDeleting(task.getState());
    state.setTask(task);
    task.setState(state);
    task.setDeveloper(null);
    log.info(String.format(" %s moved to deleted state", task));
  }

  @Override
  public void setTask(Task task) {
    this.task = task;
  }

  @Override
  public String toString() {
    return "ToDo";
  }
}