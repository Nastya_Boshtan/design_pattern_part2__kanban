package com.epam.model.state.impl;

import com.epam.model.Developer;
import com.epam.model.Task;
import com.epam.model.enums.Position;
import com.epam.model.state.State;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Deleted implements State {

  private static Logger log = LogManager.getLogger(ToDo.class);
  private State stateBeforeDeleting;
  private Task task;

  @Override
  public void restore(Task task, Developer developer) {
    if (developer.getPosition() == Position.TEAM_LEAD) {
      State state = stateBeforeDeleting;
      state.setTask(task);
      task.setState(state);
      log.info(String.format("Return to %s - state before deleting", state));
    } else {
      log.warn("Position must be TEAM_LEAD");
    }
  }

  @Override
  public void setTask(Task task) {
    this.task = task;
  }

  public State getStateBeforeDeleting() {
    return stateBeforeDeleting;
  }

  public void setStateBeforeDeleting(State stateBeforeDeleting) {
    this.stateBeforeDeleting = stateBeforeDeleting;
  }

  @Override
  public String toString() {
    return "Deleted";
  }
}
